#include <Arduino.h>

/**
 * Enregistre le message recu actuel, étant donné que le téléphone n'envoie qu'un caractère à la fois.
 */
String current = "";

/**
 * Variable qui descend de 1 tout le temps, et permet d'attendre un peu avant de considérer un message comme reçu
 */
int waiting = 0;

/**
 * Permet d'empêcher d'envoyer des messages de test envoyés lors d'envoi de messages en tant que message envoyé.
 */
boolean sent = false;

String name;
int weight;
int amount;

void setup() {
	name = "Stockage";
	weight = 100;
	amount = 1;
	Serial.begin(115200);
	Serial2.begin(9600);
	Serial.println("Done doing setup");
}

void loop() {
	// Lire l'input BT, et l'ajouter au message actuel
	if (Serial2.available()) {
		char received = char(Serial2.read());
		current += received;
		waiting = 400;
	}

	// Si on est en train de recevoir un message
	if (waiting > 0) {
		waiting--;
		if (waiting == 0) {
			Serial.println("Received \"" + current + "\"");
			if (current.startsWith("W:")) {
				current.replace("W:", "");
				Serial.println("Weight is " + current);
				weight = current.toInt();
			} else if (current.startsWith("A:")) {
				current.replace("A:", "");
				Serial.println("Amount is " + current);
				amount = current.toInt();
			} else if (current.startsWith("send")) {
				Serial.println("Sending data...");
				Serial2.print("N:" + name);
				delay(200);
				Serial2.print("A:" +  String(amount));
				delay(200);
				Serial2.print("W:" + String(weight));
			}
			current = "";
		}

		// Si on ne recoit aucun message et on veut envoyer quelque chose
	} else if (Serial.available()) {
		if (!sent) {
			String msg = Serial.readString();
			msg.replace("\n", "");
			Serial2.print(msg);
			Serial.println("Sent \"" + msg + "\"");
			sent = true;
		} else
			sent = false;
	}
}
